# Rapid-tracker
(Rapid App Dev Tri 3/2017 - 2018)

An example android app for Rapid Dev class

## Goal:
This assignment is designed to expose students to native Android app development process and tools 

with the focus on built-in capabilities of modern mobile devices such as GPS, touch gesture, etc. 

## Task: 
You will be implement an application that tracks real-time location of users. 

You will be using again Firebase real-time DB as your backend. 

The starter code is provided here: https://github.com/sunsern/rapid-tracker 

## Useful information:

* https://developers.google.com/maps/documentation/android-sdk/marker 